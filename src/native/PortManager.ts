export interface PortManager {
    attach(app: App): void;
}

export interface App {
    ports: {[name: string]: Port}
}

export interface Port {
    subscribe(fn : (...args: any[]) => void): void
    send(data: any): void
}