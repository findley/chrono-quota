import * as firebase from 'firebase/app';
import { Auth, AppUser } from './Auth';
import { App } from './PortManager';

const portManagers = [
    new Auth()
];

let app : App;

export function start(elm: any) {
    firebase.initializeApp({
        apiKey: "AIzaSyDe3sS3aiF-2ETpFAZ-oCP2NicK7BEIxfs",
        authDomain: "chrono-quota.firebaseapp.com",
        databaseURL: "https://chrono-quota.firebaseio.com",
        projectId: "chrono-quota",
        storageBucket: "chrono-quota.appspot.com",
        messagingSenderId: "338234257002",
        appId: "1:338234257002:web:7c1c16383eafe357fe6413",
        measurementId: "G-R2BGM239ED"
    });

    firebase.auth().onAuthStateChanged((user: firebase.User | null) => {
        if (!app) {
            console.log(user);
            const appUser = user ? Auth.makeAppUser(user, "", false) : null;
            console.log(appUser);
            app = init(elm, appUser);
        }
    });
}

function init(elm, user: AppUser | null): App {
    const app = elm.Main.init({ node: document.getElementById('app'), flags: user });
    portManagers.forEach(manager => manager.attach(app));

    return app;
}