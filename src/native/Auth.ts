import * as firebase from 'firebase/app';
import 'firebase/auth';
import { PortManager, App } from "./PortManager";

export class Auth implements PortManager {
    private app: App;

    public attach(app: App): void {
        this.app = app;
        app.ports.authOut.subscribe(data => this.handleAuthRequest(data));
    }

    private handleAuthRequest(data: AuthRequest): void {
        switch (data.type) {
            case 'sign-in-with-email':
                this.signInWithEmail(data as EmailSignInRequest);
                break;
            case 'sign-in-with-google':
                this.signInWithGoogle(data as GoogleSignInRequest);
                break;
            case 'create-email-account':
                this.createEmailAccount(data as CreateEmailAccountRequest);
                break;
            case 'sign-out':
                this.signOut();
                break;
        }
    }

    private signInWithEmail(request: EmailSignInRequest) {
        firebase.auth().signInWithEmailAndPassword(request.email, request.password)
        .then(this.handleSuccessfulSignIn.bind(this))
        .catch(this.handleFailedSignIn.bind(this));
    }

    private signInWithGoogle(request: GoogleSignInRequest) {
        const provider = new firebase.auth.GoogleAuthProvider()
        firebase.auth().signInWithPopup(provider)
        .then(this.handleSuccessfulSignIn.bind(this))
        .catch(this.handleFailedSignIn.bind(this));
    }

    private createEmailAccount(request: CreateEmailAccountRequest) {
        firebase.auth().createUserWithEmailAndPassword(request.email, request.password)
        .then((creds: firebase.auth.UserCredential) => {
            this.handleSuccessfulSignIn(creds);
            creds.user.updateProfile({displayName: request.name});
        })
        .catch(this.handleFailedSignIn.bind(this));
    }

    private signOut() {
        console.log('Got sign-out request');
    }

    private handleSuccessfulSignIn(cred: firebase.auth.UserCredential) {
        console.log(cred);
        this.app.ports.authIn.send({
            code: 'auth/success',
            data: Auth.makeAppUser(
                cred.user,
                cred.additionalUserInfo.providerId,
                cred.additionalUserInfo.isNewUser
            )
        });
    }

    private handleFailedSignIn(err: firebase.FirebaseError) {
        console.log(err);
        this.app.ports.authIn.send({ code: err.code, data: err.message });
    }

    public static makeAppUser(user: firebase.User, authProvider: string, isNewUser: boolean): AppUser {
        return {
            uid: user.uid,
            email: user.email,
            emailVerified: user.emailVerified,
            displayName: user.displayName,
            photoUrl: user.photoURL,
            authProvider: authProvider,
            isNewUser: isNewUser,
        };
    }
}

interface AuthRequest {
    type: string;
}

interface EmailSignInRequest extends AuthRequest {
    email: string;
    password: string;
}

interface GoogleSignInRequest extends AuthRequest {}

interface CreateEmailAccountRequest extends AuthRequest {
    name: string;
    email: string;
    password: string;
}

export interface AppUser {
    uid: string,
    email: string,
    emailVerified: boolean,
    displayName: string,
    photoUrl: string,
    authProvider: string,
    isNewUser: boolean
}