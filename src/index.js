import { Elm } from './elm/Main.elm';
import * as bootstrap from './native/bootstrap.ts';

bootstrap.start(Elm);
