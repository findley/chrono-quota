module Page.Home exposing (Model, Msg, init, subscriptions, update, view)

import Auth
import Browser.Navigation as Nav
import Duration
import FormatNumber
import FormatNumber.Locales exposing (usLocale)
import Html
import Html.Attributes as Attrs
import Html.Events as Events
import Quota exposing (Quota)
import Route
import Task
import Time



-- MODEL


type alias Model =
    { quotas : List Quota
    , time : Time.Posix
    , zone : Time.Zone
    , user : Auth.User
    , navKey : Nav.Key
    }


init : Nav.Key -> Auth.User -> ( Model, Cmd Msg )
init key user =
    let
        q1 =
            Quota.newQuota "Review PRs" (Quota.Duration <| Duration.minutes 1)

        q2 =
            Quota.newQuota "Meetings" (Quota.Duration <| Duration.hours 1)
    in
    ( Model
        [ q1, q2 ]
        (Time.millisToPosix 0)
        Time.utc
        user
        key
    , Cmd.batch [ Task.perform Tick Time.now, Task.perform Timezone Time.here ]
    )



-- UPDATE


type Msg
    = ClickQuota String
    | Tick Time.Posix
    | Timezone Time.Zone


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ClickQuota id ->
            ( { model | quotas = clickQuota id model.quotas model.time }, Cmd.none )

        Tick time ->
            ( { model | time = time }, Cmd.none )

        Timezone zone ->
            ( { model | zone = zone }, Cmd.none )


clickQuota : String -> List Quota.Quota -> Time.Posix -> List Quota.Quota
clickQuota id quotas time =
    let
        updateQuota quota =
            if quota.id == id then
                clickQuota2 quota time

            else
                quota
    in
    List.map updateQuota quotas


clickQuota2 : Quota.Quota -> Time.Posix -> Quota.Quota
clickQuota2 task time =
    case task.quantity of
        Quota.Duration _ ->
            case task.state of
                Quota.Running startTime ->
                    { task | state = Quota.Stopped, actions = Quota.TimePair startTime time :: task.actions }

                Quota.Stopped ->
                    { task | state = Quota.Running time }

        Quota.Count _ ->
            { task | actions = Quota.Check time :: task.actions }



-- VIEW


view : Model -> { title : String, content : Html.Html Msg }
view model =
    let
        quotaViewer =
            viewQuota model.time model.zone
    in
    { title = "Chrono Quota"
    , content =
        Html.div []
            [ viewAppBar
            , Html.div [ Attrs.class "home-container" ] (List.map quotaViewer model.quotas)
            ]
    }


viewQuota : Time.Posix -> Time.Zone -> Quota -> Html.Html Msg
viewQuota time zone quota =
    let
        actionSum =
            sumActions quota time zone

        percent =
            viewProgressPercent quota actionSum

        progress =
            viewProgressAmount quota actionSum

        inactive =
            if Maybe.withDefault 0 (String.toFloat percent) >= 100 then
                True

            else
                False
    in
    Html.div [ Attrs.classList [ ( "quota-container", True ), ( "quota-inactive", inactive ) ] ]
        [ viewQuotaLabel quota progress
        , viewQuotaBar quota percent
        ]


viewQuotaLabel : Quota -> String -> Html.Html Msg
viewQuotaLabel quota progress =
    Html.div [ Attrs.class "quota-label" ]
        [ Html.span [ Events.onClick (ClickQuota quota.id), Attrs.class "quota-name" ]
            [ viewQuotaButton quota.state
            , Html.text (quota.label ++ " - " ++ progress)
            ]
        , Html.span [ Attrs.class "quota-recurrence" ] [ viewRecurrence quota ]
        , Html.span [ Attrs.class "quota-edit-link" ] [ Html.a [ Attrs.href "#" ] [ Html.text "Edit" ] ]
        ]


viewRecurrence : Quota -> Html.Html Msg
viewRecurrence quota =
    let
        total =
            case quota.quantity of
                Quota.Duration d ->
                    Duration.inHuman d

                Quota.Count x ->
                    String.fromInt x ++ "x"
    in
    case quota.recurrence of
        Quota.Once ->
            Html.text (total ++ "One Time")

        Quota.Daily _ ->
            Html.text <| total ++ " Daily"

        Quota.Weekly ->
            Html.text (total ++ " Weekly")

        Quota.Monthly ->
            Html.text (total ++ " Monthly")


viewQuotaBar : Quota -> String -> Html.Html Msg
viewQuotaBar quota progress =
    Html.div [ Attrs.class "quota-bar", Events.onClick (ClickQuota quota.id) ]
        [ Html.div [ Attrs.class "quota-bar-progress", Attrs.style "width" (progress ++ "%") ] [] ]


viewQuotaButton : Quota.State -> Html.Html Msg
viewQuotaButton state =
    let
        c =
            case state of
                Quota.Running _ ->
                    String.fromChar '■'

                _ ->
                    String.fromChar '▶'
    in
    Html.div [ Attrs.class "quota-label-button" ] [ Html.text c ]


sumActions : Quota.Quota -> Time.Posix -> Time.Zone -> Int
sumActions quota time zone =
    let
        period =
            Quota.currentPeriod quota zone time

        actions =
            Quota.actionsInPeriod quota period

        sum =
            Quota.sumActions actions
    in
    case quota.quantity of
        Quota.Duration _ ->
            let
                realTime =
                    case quota.state of
                        Quota.Running inTime ->
                            Time.posixToMillis time - Time.posixToMillis inTime

                        _ ->
                            0
            in
            sum + realTime

        Quota.Count _ ->
            sum


viewProgressAmount : Quota.Quota -> Int -> String
viewProgressAmount quota actionSum =
    case quota.quantity of
        Quota.Duration _ ->
            actionSum |> Duration.milliseconds |> Duration.inHuman

        Quota.Count _ ->
            String.fromInt actionSum ++ "x"


viewProgressPercent : Quota.Quota -> Int -> String
viewProgressPercent quota actionSum =
    let
        percent =
            case quota.quantity of
                Quota.Duration duration ->
                    toFloat actionSum / toFloat (Duration.inMilliseconds duration) * 100

                Quota.Count times ->
                    toFloat actionSum / toFloat times * 100
    in
    FormatNumber.format { usLocale | decimals = 2 } (clamp 0 100 percent)


viewAppBar : Html.Html Msg
viewAppBar =
    Html.div [ Attrs.class "app-bar" ]
        [ Html.div [ Attrs.class "app-bar-logo" ] [ Html.text "Chrono Quota" ]
        , Html.div [] []
        , Html.div [ Attrs.class "app-bar-buttons" ]
            [ Html.div [ Attrs.class "app-bar-add" ] [ Html.text "+" ]
            ]
        ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Time.every 1000 Tick
