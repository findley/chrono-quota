module Page.Register exposing (Model, Msg(..), init, subscriptions, update, view)

import Auth
import Browser.Navigation as Nav
import Html
import Html.Attributes as Attrs
import Html.Events as Events
import Route



-- MODEL


type alias Model =
    { name : String
    , email : String
    , password : String
    , passwordAgain : String
    , processing : Bool
    , error : Maybe String
    , navKey : Nav.Key
    }


init : Nav.Key -> ( Model, Cmd Msg )
init navKey =
    ( Model "" "" "" "" False Nothing navKey, Cmd.none )



-- UPDATE


type Msg
    = ChangeName String
    | ChangeEmail String
    | ChangePassword String
    | ChangePasswordAgain String
    | GoogleLogin
    | SubmitRegister
    | Auth Auth.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeName name ->
            ( { model | name = name }, Cmd.none )

        ChangeEmail email ->
            ( { model | email = email }, Cmd.none )

        ChangePassword password ->
            ( { model | password = password }, Cmd.none )

        ChangePasswordAgain passwordAgain ->
            ( { model | passwordAgain = passwordAgain }, Cmd.none )

        GoogleLogin ->
            ( { model | processing = True }, Auth.signInWithGoogle )

        SubmitRegister ->
            ( { model | processing = True }, Auth.createEmailAccount model.name model.email model.password )

        Auth authMsg ->
            case authMsg of
                Auth.Authenticated _ ->
                    ( model, Route.pushUrl model.navKey Route.homeRoute )

                Auth.AuthFailed err ->
                    ( { model | processing = False, error = Just (Auth.errorToString err) }, Cmd.none )



-- VIEW


view : Model -> { title : String, content : Html.Html Msg }
view model =
    { title = "Register - Chrono Quota"
    , content =
        Html.div [ Attrs.class "container" ]
            [ Html.div [ Attrs.class "login-content" ]
                [ viewTitle
                , viewRegisterForm model
                , viewSignInError model.error model.processing
                , viewOrSeparator
                , viewGoogleSignIn model.processing
                ]
            ]
    }


viewTitle : Html.Html Msg
viewTitle =
    Html.div [ Attrs.class "title-img" ]
        [ Html.img
            [ Attrs.alt "Chrono Quota"
            , Attrs.src "./img/chronoquota.png"
            ]
            []
        ]


viewSignInError : Maybe String -> Bool -> Html.Html Msg
viewSignInError error processing =
    case error of
        Nothing ->
            Html.text ""

        Just text ->
            if processing then
                Html.text ""

            else
                Html.div [ Attrs.class "sign-in-error" ] [ Html.text text ]


viewRegisterForm : Model -> Html.Html Msg
viewRegisterForm model =
    Html.form [ Attrs.class "login-form", Events.onSubmit SubmitRegister ]
        [ viewNameInput model.name
        , viewEmailInput model.email
        , viewPasswordInput model.password
        , viewPasswordAgainInput model.passwordAgain
        , viewRegisterButton model.processing
        , viewExtraLinks
        ]


viewNameInput : String -> Html.Html Msg
viewNameInput name =
    Html.div []
        [ Html.input
            [ Attrs.id ""
            , Attrs.type_ "text"
            , Attrs.placeholder "name"
            , Attrs.autofocus True
            , Attrs.required True
            , Attrs.value name
            , Events.onInput ChangeName
            ]
            []
        ]


viewEmailInput : String -> Html.Html Msg
viewEmailInput email =
    Html.div []
        [ Html.input
            [ Attrs.id "email-input"
            , Attrs.type_ "email"
            , Attrs.placeholder "email"
            , Attrs.required True
            , Attrs.value email
            , Events.onInput ChangeEmail
            ]
            []
        ]


viewPasswordInput : String -> Html.Html Msg
viewPasswordInput password =
    Html.div []
        [ Html.input
            [ Attrs.id "password-input"
            , Attrs.type_ "password"
            , Attrs.placeholder "password"
            , Attrs.required True
            , Attrs.value password
            , Events.onInput ChangePassword
            ]
            []
        ]


viewPasswordAgainInput : String -> Html.Html Msg
viewPasswordAgainInput password =
    Html.div []
        [ Html.input
            [ Attrs.id "password-again-input"
            , Attrs.type_ "password"
            , Attrs.placeholder "password again"
            , Attrs.required True
            , Attrs.value password
            , Events.onInput ChangePasswordAgain
            ]
            []
        ]


viewRegisterButton : Bool -> Html.Html Msg
viewRegisterButton processing =
    Html.div []
        [ Html.button
            [ Attrs.type_ "submit", Attrs.disabled processing ]
            [ Html.text "register ->" ]
        ]


viewExtraLinks : Html.Html Msg
viewExtraLinks =
    Html.div [ Attrs.class "sign-in-extra-links" ]
        [ Html.a [ Route.href Route.loginRoute ] [ Html.text "Already have an account?" ]
        ]


viewOrSeparator : Html.Html Msg
viewOrSeparator =
    Html.hr [ Attrs.class "login-separator" ] []


viewGoogleSignIn : Bool -> Html.Html Msg
viewGoogleSignIn processing =
    Html.button
        [ Attrs.class "google-sign-in"
        , Attrs.disabled processing
        , Events.onClick GoogleLogin
        ]
        [ Html.img [ Attrs.src "img/btn_google_light_normal_ios.svg" ] []
        , Html.text "Sign in with Google ->"
        ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Auth.authenticated (\authMsg -> Auth authMsg)
