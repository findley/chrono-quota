module Page.Login exposing (Model, Msg(..), init, subscriptions, update, view)

import Auth
import Browser.Navigation as Nav
import Html
import Html.Attributes as Attrs
import Html.Events as Events
import Route



-- MODEL


type alias Model =
    { email : String
    , password : String
    , processing : Bool
    , error : Maybe String
    , navKey : Nav.Key
    }


init : Nav.Key -> ( Model, Cmd Msg )
init key =
    ( Model "" "" False Nothing key, Cmd.none )



-- UPDATE


type Msg
    = ChangeEmail String
    | ChangePassword String
    | EmailLogin
    | GoogleLogin
    | Auth Auth.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangeEmail email ->
            ( { model | email = email }, Cmd.none )

        ChangePassword password ->
            ( { model | password = password }, Cmd.none )

        EmailLogin ->
            ( { model | processing = True }, Auth.signInWithEmail model.email model.password )

        GoogleLogin ->
            ( { model | processing = True }, Auth.signInWithGoogle )

        Auth authMsg ->
            case authMsg of
                Auth.Authenticated _ ->
                    ( model, Route.pushUrl model.navKey Route.homeRoute )

                Auth.AuthFailed err ->
                    ( { model | processing = False, error = Just (Auth.errorToString err) }, Cmd.none )



-- VIEW


view : Model -> { title : String, content : Html.Html Msg }
view model =
    { title = "Login - Chrono Quota"
    , content =
        Html.div [ Attrs.class "container" ]
            [ Html.div [ Attrs.class "login-content" ]
                [ viewTitle
                , viewSignInForm model
                , viewOrSeparator
                , viewGoogleSignIn model.processing
                ]
            ]
    }


viewTitle : Html.Html Msg
viewTitle =
    Html.div [ Attrs.class "title-img" ]
        [ Html.img
            [ Attrs.alt "Chrono Quota"
            , Attrs.src "./img/chronoquota.png"
            ]
            []
        ]


viewSignInError : Maybe String -> Bool -> Html.Html Msg
viewSignInError error processing =
    case error of
        Nothing ->
            Html.text ""

        Just text ->
            if processing then
                Html.text ""

            else
                Html.div [ Attrs.class "sign-in-error" ] [ Html.text text ]


viewSignInForm : Model -> Html.Html Msg
viewSignInForm model =
    Html.form [ Attrs.class "login-form", Events.onSubmit EmailLogin ]
        [ viewEmailInput model.email
        , viewPasswordInput model.password
        , viewSignInButton model.processing
        , viewSignInError model.error model.processing
        , viewExtraLinks
        ]


viewEmailInput : String -> Html.Html Msg
viewEmailInput email =
    Html.div []
        [ Html.input
            [ Attrs.id "email-input"
            , Attrs.type_ "email"
            , Attrs.placeholder "email"
            , Attrs.autofocus True
            , Attrs.required True
            , Attrs.value email
            , Events.onInput ChangeEmail
            ]
            []
        ]


viewPasswordInput : String -> Html.Html Msg
viewPasswordInput password =
    Html.div []
        [ Html.input
            [ Attrs.id "password-input"
            , Attrs.type_ "password"
            , Attrs.placeholder "password"
            , Attrs.required True
            , Attrs.value password
            , Events.onInput ChangePassword
            ]
            []
        ]


viewSignInButton : Bool -> Html.Html Msg
viewSignInButton processing =
    Html.div []
        [ Html.button
            [ Attrs.type_ "submit", Attrs.disabled processing ]
            [ Html.text "login ->" ]
        ]


viewExtraLinks : Html.Html Msg
viewExtraLinks =
    Html.div [ Attrs.class "sign-in-extra-links" ]
        [ Html.a [ Route.href Route.registerRoute ] [ Html.text "Create Account" ]
        , Html.a [ Attrs.href "#" ] [ Html.text "Reset Password" ]
        ]


viewOrSeparator : Html.Html Msg
viewOrSeparator =
    Html.hr [ Attrs.class "login-separator" ] []


viewGoogleSignIn : Bool -> Html.Html Msg
viewGoogleSignIn processing =
    Html.button
        [ Attrs.class "google-sign-in"
        , Attrs.disabled processing
        , Events.onClick GoogleLogin
        ]
        [ Html.img [ Attrs.src "img/btn_google_light_normal_ios.svg" ] []
        , Html.text "Sign in with Google ->"
        ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Auth.authenticated (\authMsg -> Auth authMsg)
