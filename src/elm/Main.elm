module Main exposing (main)

import Auth
import Browser
import Browser.Navigation as Nav
import Html
import Json.Encode as E
import Page.Home as Home
import Page.Login as Login
import Page.Register as Register
import Route exposing (Route)
import Url



-- MAIN


main : Program E.Value Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = UrlRequested
        , onUrlChange = UrlChanged
        }



-- MODEL


type alias Model =
    { key : Nav.Key
    , url : Url.Url
    , user : Maybe Auth.User
    , page : Page
    }


type Page
    = Login Login.Model
    | Register Register.Model
    | Home Home.Model
    | NotFound
    | Redirect


init : E.Value -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init maybeUser url key =
    let
        user =
            Auth.decodeUser maybeUser |> Result.toMaybe

        route =
            Route.fromUrl url
    in
    changeRouteTo route (Model key url user Redirect)



-- UPDATE


type Msg
    = UrlRequested Browser.UrlRequest
    | UrlChanged Url.Url
    | LoginMsg Login.Msg
    | RegisterMsg Register.Msg
    | HomeMsg Home.Msg
    | Auth Auth.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model.page ) of
        ( UrlRequested urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        ( UrlChanged url, _ ) ->
            changeRouteTo (Route.fromUrl url) { model | url = url }

        ( Auth authMsg, _ ) ->
            case authMsg of
                Auth.Authenticated user ->
                    ( { model | user = Just user }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        ( LoginMsg loginMsg, Login loginModel ) ->
            Login.update loginMsg loginModel |> updateWith Login LoginMsg model

        ( RegisterMsg registerMsg, Register registerModel ) ->
            Register.update registerMsg registerModel |> updateWith Register RegisterMsg model

        ( HomeMsg homeMsg, Home homeModel ) ->
            Home.update homeMsg homeModel |> updateWith Home HomeMsg model

        ( _, _ ) ->
            -- Disregard messages that arrive for the wrong page.
            ( model, Cmd.none )


updateWith : (subModel -> Page) -> (subMsg -> Msg) -> Model -> ( subModel, Cmd subMsg ) -> ( Model, Cmd Msg )
updateWith toModel toMsg model ( subModel, subCmd ) =
    ( { model | page = toModel subModel }, Cmd.map toMsg subCmd )


changeRouteTo : Maybe Route -> Model -> ( Model, Cmd Msg )
changeRouteTo route model =
    case ( route, model.user ) of
        ( Nothing, _ ) ->
            ( { model | page = NotFound }, Cmd.none )

        ( Just Route.NotFound, _ ) ->
            ( { model | page = NotFound }, Cmd.none )

        ( Just Route.Root, Nothing ) ->
            ( model, Route.replaceUrl model.key Route.loginRoute )

        ( Just Route.Root, Just _ ) ->
            ( model, Route.replaceUrl model.key Route.homeRoute )

        ( Just (Route.Authenticated _), Nothing ) ->
            ( model, Route.replaceUrl model.key Route.loginRoute )

        ( Just (Route.Unauthenticated _), Just _ ) ->
            ( model, Route.replaceUrl model.key Route.homeRoute )

        ( Just (Route.Authenticated authRoute), Just user ) ->
            case authRoute of
                Route.Home ->
                    Home.init model.key user |> updateWith Home HomeMsg model

        ( Just (Route.Unauthenticated unauthRoute), Nothing ) ->
            case unauthRoute of
                Route.Login ->
                    Login.init model.key |> updateWith Login LoginMsg model

                Route.Register ->
                    Register.init model.key |> updateWith Register RegisterMsg model



--SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    let
        pageSub =
            case model.page of
                Login loginModel ->
                    Sub.map LoginMsg (Login.subscriptions loginModel)

                Register registerModel ->
                    Sub.map RegisterMsg (Register.subscriptions registerModel)

                Home homeModel ->
                    Sub.map HomeMsg (Home.subscriptions homeModel)

                _ ->
                    Sub.none
    in
    Sub.batch
        [ pageSub
        , Auth.authenticated (\authMsg -> Auth authMsg)
        ]



--VIEW


view : Model -> Browser.Document Msg
view model =
    let
        viewPage toMsg { title, content } =
            { title = title
            , body = [ Html.map toMsg content ]
            }
    in
    case model.page of
        Login loginModel ->
            viewPage LoginMsg (Login.view loginModel)

        Register registerModel ->
            viewPage RegisterMsg (Register.view registerModel)

        Redirect ->
            { title = "Redirect", body = [ Html.text "Oopsie" ] }

        NotFound ->
            { title = "Page not found", body = [ Html.text "Page not found" ] }

        Home homeModel ->
            viewPage HomeMsg (Home.view homeModel)
