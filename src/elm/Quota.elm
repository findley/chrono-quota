module Quota exposing (..)

import Duration as D
import Json.Encode as E
import MD5
import Time



-- MODEL


type alias Quota =
    { id : String
    , label : String
    , quantity : Quantity
    , state : State
    , recurrence : Recurrence
    , actions : List Action
    , expanded : Bool
    }


type Recurrence
    = Once
    | Daily (List Time.Weekday)
    | Weekly
    | Monthly


type Action
    = Check Time.Posix
    | TimePair Time.Posix Time.Posix


type State
    = Running Time.Posix
    | Stopped


type Quantity
    = Duration D.Duration
    | Count Int



-- HELPERS


quantityAmount : Quantity -> Int
quantityAmount q =
    case q of
        Duration d ->
            D.inMilliseconds d

        Count x ->
            x


activeQuotas : List Quota -> Time.Zone -> Time.Posix -> List Quota
activeQuotas quotas zone time =
    List.filter (\q -> isQuotaActive q zone time) quotas


isQuotaActive : Quota -> Time.Zone -> Time.Posix -> Bool
isQuotaActive quota zone time =
    case quota.recurrence of
        Daily days ->
            let
                dow =
                    Time.toWeekday zone time
            in
            List.member dow days

        _ ->
            -- TODO: Finish implementation
            True


currentPeriod : Quota -> Time.Zone -> Time.Posix -> ( Time.Posix, Time.Posix )
currentPeriod quota zone time =
    case quota.recurrence of
        Once ->
            ( Time.millisToPosix 0, Time.millisToPosix 32503680000000 )

        -- Year 3000
        Daily _ ->
            let
                hour =
                    Time.toHour zone time

                minute =
                    Time.toMinute zone time

                second =
                    Time.toSecond zone time

                offset =
                    hour * 60 * 60 * 1000 + minute * 60 * 1000 + second

                startOfDay =
                    Time.posixToMillis time - offset

                endOfDay =
                    startOfDay + 24 * 60 * 60 * 1000 - 1
            in
            ( Time.millisToPosix startOfDay, Time.millisToPosix endOfDay )

        _ ->
            -- TODO: Finish implementation
            ( Time.millisToPosix 0, Time.millisToPosix 32503680000000 )


actionsInPeriod : Quota -> ( Time.Posix, Time.Posix ) -> List Action
actionsInPeriod quota ( start, end ) =
    List.filter (\a -> actionInPeriod a ( start, end )) quota.actions


actionInPeriod : Action -> ( Time.Posix, Time.Posix ) -> Bool
actionInPeriod action ( start, end ) =
    case action of
        Check time ->
            Time.posixToMillis start <= Time.posixToMillis time && Time.posixToMillis time <= Time.posixToMillis end

        TimePair time _ ->
            Time.posixToMillis start <= Time.posixToMillis time && Time.posixToMillis time <= Time.posixToMillis end


sumActions : List Action -> Int
sumActions actions =
    List.foldl (\action total -> total + actionQuantity action) 0 actions


actionQuantity : Action -> Int
actionQuantity action =
    case action of
        Check _ ->
            1

        TimePair start end ->
            Time.posixToMillis end - Time.posixToMillis start


newQuota : String -> Quantity -> Quota
newQuota label quantity =
    Quota
        (MD5.hex (label ++ String.fromInt (quantityAmount quantity)))
        label
        quantity
        Stopped
        (Daily [ Time.Wed, Time.Sat ])
        []
        False



-- ENCODE


encodeQuota : Quota -> E.Value
encodeQuota quota =
    E.object
        [ ( "id", E.string quota.id )
        , ( "label", E.string quota.label )
        , ( "quantity", encodeQuantity quota.quantity )
        , ( "state", encodeState quota.state )
        , ( "recurrence", encodeRecurrence quota.recurrence )
        , ( "actions", E.list encodeAction quota.actions )
        , ( "expanded", E.bool quota.expanded )
        ]


encodeQuantity : Quantity -> E.Value
encodeQuantity quantity =
    case quantity of
        Duration d ->
            E.object
                [ ( "type", E.string "Duration" )
                , ( "value", E.int (D.inMilliseconds d) )
                ]

        Count c ->
            E.object [ ( "type", E.string "Count" ), ( "value", E.int c ) ]


encodeState : State -> E.Value
encodeState state =
    case state of
        Running time ->
            E.object
                [ ( "type", E.string "Running" )
                , ( "value", E.int (Time.posixToMillis time) )
                ]

        Stopped ->
            E.object [ ( "type", E.string "Stopped" ) ]


encodeRecurrence : Recurrence -> E.Value
encodeRecurrence recurrence =
    case recurrence of
        Once ->
            E.object [ ( "type", E.string "Once" ) ]

        Daily days ->
            E.object
                [ ( "type", E.string "Daily" )
                , ( "days", E.list encodeWeekday days )
                ]

        Weekly ->
            E.object [ ( "type", E.string "Weekly" ) ]

        Monthly ->
            E.object [ ( "type", E.string "Monthly" ) ]


encodeWeekday : Time.Weekday -> E.Value
encodeWeekday day =
    case day of
        Time.Sun ->
            E.string "Sun"

        Time.Mon ->
            E.string "Mon"

        Time.Tue ->
            E.string "Tue"

        Time.Wed ->
            E.string "Wed"

        Time.Thu ->
            E.string "Thu"

        Time.Fri ->
            E.string "Fri"

        Time.Sat ->
            E.string "Sat"


encodeAction : Action -> E.Value
encodeAction action =
    case action of
        Check time ->
            E.object
                [ ( "type", E.string "Check" )
                , ( "value", E.int (Time.posixToMillis time) )
                ]

        TimePair start end ->
            E.object
                [ ( "type", E.string "TimePair" )
                , ( "start", E.int (Time.posixToMillis start) )
                , ( "end", E.int (Time.posixToMillis end) )
                ]
