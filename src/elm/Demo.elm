port module Demo exposing (..)

import Browser
import Debug
import Duration
import FormatNumber
import FormatNumber.Locales exposing (usLocale)
import Html
import Html.Attributes as Attrs
import Html.Events as Events
import Json.Encode as E
import Quota
import Task exposing (perform)
import Time



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = updateWithStorage
        , subscriptions = subscriptions
        }


port setStorage : E.Value -> Cmd msg


updateWithStorage : Msg -> Model -> ( Model, Cmd Msg )
updateWithStorage msg model =
    let
        ( newModel, cmds ) =
            update msg model
    in
    ( newModel
    , Cmd.batch [ setStorage (encodeModel newModel), cmds ]
    )



-- MODEL


type alias Model =
    { quotas : List Quota.Quota
    , time : Time.Posix
    , zone : Time.Zone
    }


init : () -> ( Model, Cmd Msg )
init _ =
    let
        q1 =
            Quota.newQuota "Review PRs" (Quota.Count 5)

        q2 =
            Quota.newQuota "Meetings" (Quota.Duration <| Duration.hours 1)
    in
    ( Model
        [ q1, q2 ]
        (Time.millisToPosix 0)
        Time.utc
    , Cmd.batch [ perform Tick Time.now, perform Timezone Time.here ]
    )


encodeModel : Model -> E.Value
encodeModel model =
    E.object [ ( "quotas", E.list Quota.encodeQuota model.quotas ) ]



-- UPDATE


type Msg
    = ClickQuota String
    | Tick Time.Posix
    | Timezone Time.Zone


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ClickQuota id ->
            ( { model | quotas = clickQuota id model.quotas model.time }, Cmd.none )

        Tick time ->
            ( { model | time = Debug.log "time" time }, Cmd.none )

        Timezone zone ->
            ( { model | zone = Debug.log "zone" zone }, Cmd.none )


clickQuota : String -> List Quota.Quota -> Time.Posix -> List Quota.Quota
clickQuota id quotas time =
    let
        updateQuota quota =
            if quota.id == id then
                clickQuota2 quota time

            else
                quota
    in
    List.map updateQuota quotas


clickQuota2 : Quota.Quota -> Time.Posix -> Quota.Quota
clickQuota2 task time =
    case task.quantity of
        Quota.Duration _ ->
            case task.state of
                Quota.Running startTime ->
                    { task | state = Quota.Stopped, actions = Quota.TimePair startTime time :: task.actions }

                Quota.Stopped ->
                    { task | state = Quota.Running time }

        Quota.Count _ ->
            { task | actions = Quota.Check time :: task.actions }



-- VIEW


view : Model -> Html.Html Msg
view model =
    let
        quotas =
            Quota.activeQuotas model.quotas model.zone model.time
    in
    Html.div [ Attrs.class "container" ]
        [ Html.div [ Attrs.class "content" ]
            (viewTitle :: List.map (\q -> viewQuota q model.time model.zone) quotas)
        ]


viewTitle : Html.Html Msg
viewTitle =
    Html.div [ Attrs.class "title" ] [ Html.img [ Attrs.src "img/chronoquota.png" ] [] ]


viewQuota : Quota.Quota -> Time.Posix -> Time.Zone -> Html.Html Msg
viewQuota quota time zone =
    let
        progress =
            FormatNumber.format { usLocale | decimals = 2 } (Debug.log "a" (min 100 (progressPercent quota time zone)))

        total =
            case quota.quantity of
                Quota.Duration d ->
                    Duration.inHuman d

                Quota.Count x ->
                    String.fromInt x ++ "x"
    in
    Html.div [ Attrs.class "task" ]
        [ Html.div [ Attrs.class "bar", Attrs.class "bar-active" ]
            [ Html.div [ Attrs.class "progress", Attrs.style "width" (progress ++ "%"), Events.onClick (ClickQuota quota.id) ]
                [ Html.div
                    [ Attrs.class "text" ]
                    [ Html.div [] [ viewBarButton quota.state, Html.text quota.label ]
                    , Html.div [ Attrs.style "text-align" "center" ] [ Html.text (progress ++ "%") ]
                    , Html.div [ Attrs.style "text-align" "right" ] [ Html.text total ]
                    ]
                ]
            , Html.div [ Attrs.class "expand" ] [ Html.img [ Attrs.src "img/expand.png" ] [] ]
            ]

        -- , Html.div [ Attrs.class "task-menu-icon" ] [ Html.img [ Attrs.src "img/menu-icon.png" ] [] ]
        ]


viewBarButton : Quota.State -> Html.Html Msg
viewBarButton state =
    let
        c =
            case state of
                Quota.Running _ ->
                    String.fromChar '■'

                _ ->
                    String.fromChar '▶'
    in
    Html.div [ Attrs.style "width" "25px", Attrs.style "display" "inline-block" ] [ Html.text c ]


viewQuotaForm : Quota.Quota -> Html.Html Msg
viewQuotaForm quota =
    Html.div [] []


progressPercent : Quota.Quota -> Time.Posix -> Time.Zone -> Float
progressPercent quota time zone =
    let
        period =
            Quota.currentPeriod quota zone time

        actions =
            Quota.actionsInPeriod quota period

        actionSum =
            Quota.sumActions actions
    in
    case quota.quantity of
        Quota.Duration duration ->
            let
                realTime =
                    case quota.state of
                        Quota.Running inTime ->
                            toFloat (Time.posixToMillis time - Time.posixToMillis inTime)

                        _ ->
                            0
            in
            (actionSum + realTime) / toFloat (Duration.inMilliseconds duration) * 100

        Quota.Count times ->
            actionSum / toFloat times * 100



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Time.every 1000 Tick
