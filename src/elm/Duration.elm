module Duration exposing
    ( Duration
    , hours
    , inHours
    , inHuman
    , inMilliseconds
    , inMinutes
    , inSeconds
    , milliseconds
    , minutes
    , seconds
    )


type Duration
    = Quantity Int


type Unit
    = Hour
    | Minute
    | Second


unitToMillis : Unit -> Int
unitToMillis unit =
    case unit of
        Hour ->
            60 * 60 * 1000

        Minute ->
            60 * 1000

        Second ->
            1000


unitToString : Unit -> String
unitToString unit =
    case unit of
        Hour ->
            "h"

        Minute ->
            "m"

        Second ->
            "s"


milliseconds : Int -> Duration
milliseconds ms =
    Quantity ms


seconds : Int -> Duration
seconds s =
    Quantity (s * 1000)


minutes : Int -> Duration
minutes m =
    Quantity (m * 1000 * 60)


hours : Int -> Duration
hours h =
    Quantity (h * 1000 * 60 * 60)


inMilliseconds : Duration -> Int
inMilliseconds d =
    case d of
        Quantity ms ->
            ms


inSeconds : Duration -> Int
inSeconds d =
    case d of
        Quantity ms ->
            ms // 1000


inMinutes : Duration -> Int
inMinutes d =
    case d of
        Quantity ms ->
            ms // (1000 * 60)


inHours : Duration -> Int
inHours d =
    case d of
        Quantity ms ->
            ms // (1000 * 60 * 60)


inUnits : Unit -> Duration -> Int
inUnits u d =
    case u of
        Hour ->
            inHours d

        Minute ->
            inMinutes d

        Second ->
            inSeconds d


inHuman : Duration -> String
inHuman d =
    Tuple.second <|
        List.foldl
            (\unit ( dur, result ) ->
                let
                    amount =
                        inUnits unit dur

                    nextDur =
                        milliseconds (inMilliseconds dur - amount * unitToMillis unit)

                    string =
                        if amount > 0 then
                            result ++ String.fromInt amount ++ unitToString unit

                        else
                            result
                in
                ( nextDur, string )
            )
            ( d, "" )
            [ Hour, Minute, Second ]
