port module Auth exposing
    ( Error(..)
    , Msg(..)
    , User
    , authenticated
    , createEmailAccount
    , decodeUser
    , errorToString
    , signInWithEmail
    , signInWithGoogle
    )

import Json.Decode as D
import Json.Encode as E


port authOut : E.Value -> Cmd msg


port authIn : (E.Value -> msg) -> Sub msg


type Msg
    = Authenticated User
    | AuthFailed Error


type AuthProvider
    = UnknownAuth
    | PasswordAuth
    | GoogleAuth
    | TwitterAuth


{-| Object that should be passed into the `authIn` port after
successful authentication. If successful, data should contain
a `User`
-}
type alias AuthResponse =
    { code : String
    , data : E.Value
    }


{-| Represents an authenticated user
-}
type alias User =
    { id : String
    , email : String
    , emailVerified : Bool
    , displayName : Maybe String
    , photoUrl : Maybe String
    , authProvider : AuthProvider
    , isNewUser : Bool
    }


type Error
    = AuthInvalidEmail
    | AuthUserDisabled
    | AuthUserNotFound
    | AuthWrongPassword
    | AuthUnknown
    | AuthDecodeFailed D.Error


{-| Authenticate using a username and password. This command should always
result in an `Authenticated` or `AuthFailed` message.
-}
signInWithEmail : String -> String -> Cmd msg
signInWithEmail email password =
    let
        data =
            [ ( "email", email ), ( "password", password ) ]
    in
    authRequest "sign-in-with-email" data |> authOut


{-| Authenticate using a Google account (with a popup). This command should
always result in an `Authenticated` or `AuthFailed` message.
-}
signInWithGoogle : Cmd msg
signInWithGoogle =
    authRequest "sign-in-with-google" [] |> authOut


createEmailAccount : String -> String -> String -> Cmd msg
createEmailAccount name email password =
    let
        data =
            [ ( "name", name ), ( "email", email ), ( "password", password ) ]
    in
    authRequest "create-email-account" data |> authOut


{-| Subscribe to authentication events, primarily for handling successful
or unsuccessful sign-in attempts.
-}
authenticated : (Msg -> msg) -> Sub msg
authenticated toMsg =
    authIn (\v -> msgFromAuthResponse v |> toMsg)


authRequest : String -> List ( String, String ) -> E.Value
authRequest t vals =
    let
        typeField =
            ( "type", E.string t )

        fields =
            List.map (\( k, v ) -> ( k, E.string v )) vals
    in
    E.object <| typeField :: fields


msgFromAuthResponse : E.Value -> Msg
msgFromAuthResponse v =
    case decodeAuthResponse v of
        Err decodeError ->
            AuthFailed (AuthDecodeFailed decodeError)

        Ok authResponse ->
            case authResponse.code of
                "auth/success" ->
                    msgFromUser authResponse

                "auth/invalid-email" ->
                    AuthFailed AuthInvalidEmail

                "auth/user-disabled" ->
                    AuthFailed AuthUserDisabled

                "auth/user-not-found" ->
                    AuthFailed AuthUserNotFound

                "auth/wrong-password" ->
                    AuthFailed AuthWrongPassword

                _ ->
                    AuthFailed AuthUnknown


msgFromUser : AuthResponse -> Msg
msgFromUser authResponse =
    case decodeUser authResponse.data of
        Err decodeError ->
            AuthFailed (AuthDecodeFailed decodeError)

        Ok user ->
            Authenticated user


decodeAuthResponse : D.Value -> Result D.Error AuthResponse
decodeAuthResponse jsonVal =
    D.decodeValue
        (D.map2
            AuthResponse
            (D.field "code" D.string)
            (D.field "data" D.value)
        )
        jsonVal


decodeUser : D.Value -> Result D.Error User
decodeUser jsonVal =
    D.decodeValue
        (D.map7
            User
            (D.field "uid" D.string)
            (D.field "email" D.string)
            (D.field "emailVerified" D.bool)
            (D.field "displayName" (D.maybe D.string))
            (D.field "photoUrl" (D.maybe D.string))
            (D.field "authProvider" authProviderDecoder)
            (D.field "isNewUser" D.bool)
        )
        jsonVal


authProviderDecoder : D.Decoder AuthProvider
authProviderDecoder =
    D.string
        |> D.andThen
            (\str ->
                case str of
                    "google.com" ->
                        D.succeed GoogleAuth

                    "twitter.com" ->
                        D.succeed TwitterAuth

                    "password" ->
                        D.succeed PasswordAuth

                    _ ->
                        D.succeed UnknownAuth
            )


errorToString : Error -> String
errorToString e =
    case e of
        AuthInvalidEmail ->
            "Invalid email or password"

        AuthUserDisabled ->
            "This account has been disabled"

        AuthUserNotFound ->
            "Invalid email or password"

        AuthWrongPassword ->
            "Invalid email or password"

        AuthDecodeFailed _ ->
            "There was a problem handling the server response"

        AuthUnknown ->
            "Unknown error"
