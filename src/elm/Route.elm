module Route exposing
    ( AuthenticatedRoute(..)
    , Route(..)
    , UnauthenticatedRoute(..)
    , fromUrl
    , homeRoute
    , href
    , loginRoute
    , pushUrl
    , registerRoute
    , replaceUrl
    )

import Browser.Navigation as Nav
import Html exposing (Attribute)
import Html.Attributes as Attr
import Url exposing (Url)
import Url.Parser as Parser exposing ((</>), Parser, oneOf, s, string)



-- ROUTING


type Route
    = NotFound
    | Authenticated AuthenticatedRoute
    | Unauthenticated UnauthenticatedRoute
    | Root


type AuthenticatedRoute
    = Home


type UnauthenticatedRoute
    = Login
    | Register


loginRoute : Route
loginRoute =
    Unauthenticated Login


registerRoute : Route
registerRoute =
    Unauthenticated Register


homeRoute : Route
homeRoute =
    Authenticated Home


parser : Parser (Route -> a) a
parser =
    oneOf
        [ Parser.map Root Parser.top
        , Parser.map (Authenticated Home) (s "home")
        , Parser.map (Unauthenticated Login) (s "login")
        , Parser.map (Unauthenticated Register) (s "register")
        , Parser.map NotFound (s "not-found")
        ]



-- PUBLIC HELPERS


href : Route -> Attribute msg
href targetRoute =
    Attr.href (routeToString targetRoute)


replaceUrl : Nav.Key -> Route -> Cmd msg
replaceUrl key route =
    Nav.replaceUrl key (routeToString route)


pushUrl : Nav.Key -> Route -> Cmd msg
pushUrl key route =
    Nav.pushUrl key (routeToString route)


fromUrl : Url -> Maybe Route
fromUrl url =
    url
        |> Parser.parse parser



-- INTERNAL


routeToString : Route -> String
routeToString page =
    String.join "/" (routeToPieces page)


routeToPieces : Route -> List String
routeToPieces page =
    case page of
        Root ->
            [ "" ]

        NotFound ->
            [ "not-found" ]

        Authenticated authRoute ->
            case authRoute of
                Home ->
                    [ "home" ]

        Unauthenticated unauthRoute ->
            case unauthRoute of
                Login ->
                    [ "login" ]

                Register ->
                    [ "register" ]
